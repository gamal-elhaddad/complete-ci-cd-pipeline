# Complete CI-CD Pipeline 

1- Install Docker-compose on EC2 
     [](https://gist.github.com/npearce/6f3c7826c7499587f00957fee62f8ee9)

2- Create docker-compose.yaml file.

3- Add jenkinsFile to call the jenkins-shared-lib and add some stages on it :
   - Stage to increment app version 
   - Stage to build jar file using maven 
   - Stage to build docker image and tag the name
   - Stage to push the docker image to dockerhub registery
   - Stage to run app's containers from docker-compose file then deploy it to ec2 instance 
   - Stage to commit version to gitlab  


4- Adjust jenkins-file to run docker-compose on the EC2 :
   1. Copy "docker-compose.yaml" from the repo to the EC2:
     {sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"}
   2. Create serverCmds.sh file and and put in it the docker-compose command 
   3. Copy "serverCmds.sh" from the repo to the EC2 and call it the pipeline :
      {sh "scp serverCmds.sh ${ec2Instance}:/home/ec2-user"} 
      {def shellCmd = "bash ./serverCmds.sh}
   4. Use the "shellCmd" inside the ssh command :
      {sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"}   

5- Make the Image_Name dynamic not static "Variable"
   1. Pass the image_name as a param to the serverCmds.sh file:
      {def shellCmd = "bash ./serverCmds.sh ${IMAGE_NAME}"}
   2. Export this param in the serverCmds.sh: This env var will be on the EC2 so that the docker-compose.yaml can read it
      {export IMAGE = $1}
   3. Use this "IMAGE" in the docker-compose.yaml file: 
      {image:$IMAGE}

