#!/usr/bin/env groovy
library identifier: 'jenkins-shared-library@main', retriever: modernSCM(
        [$class: 'GitSCMSource',
         remote: 'https://gitlab.com/gamal-elhaddad/jenkins-shared-lib.git',
         credentialsId: 'gitlab-token']
)


pipeline {
    agent any
    tools {
        maven 'maven3.9'
    }
    
    stages {
        stage("increment version..") {
            steps {
                script {
                    echo "incrementing app version ..."
                    sh "mvn build-helper:parse-version versions:set \
                       -DnewVersion=\\\${parsedVersion.majorVersion}.\\\${parsedVersion.minorVersion}.\\\${parsedVersion.nextIncrementalVersion} \
                       versions:commit"
                    def matcher = readFile('pom.xml') =~ '<version>(.+)</version>'
                    def version = matcher[0][1]
                    env.IMAGE_VERSION = "$version-$BUILD_NUMBER"
                    
                }
            }
        }
        stage("build jar") {
            steps {
                script {    
                     echo "building jar file ..."
                     buildJar()
                }
            }
        }
        stage("build docker image and push it to docker hub") {
            steps {
                script {   
                    echo"build image and push it ..."
                    env.IMAGE_NAME = "gamalelhaddad/from-jenkins:${IMAGE_VERSION}"
                    buildDockerImage(env.IMAGE_NAME) 
                    dockerLogin()
                    dockerPush(env.IMAGE_NAME) 
                    
                   
                }
            }
        }
        stage("deploy") {
            steps {
                script {
                    echo " deploying app to ec2....."
                    def shellCmd = "bash ./serverCmds.sh ${IMAGE_NAME}"
                    def ec2Instance = "ec2-user@54.83.89.185"
                    sshagent(['ec2-server-user']) {
                        sh "scp serverCmds.sh ${ec2Instance}:/home/ec2-user"
                        sh "scp docker-compose.yaml ${ec2Instance}:/home/ec2-user"
                        sh "ssh -o StrictHostKeyChecking=no ${ec2Instance} ${shellCmd}"
                    } 
                    
                }
            }
        }
        stage("commit version to gitlab...") {
            steps {
                script {
                    withCredentials([usernamePassword(credentialsId: 'gitlab-token', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                        sh 'git config --global user.email "jenkins@example.com"'
                        sh 'git config --global user.name "jenkins"'
                        sh "git status"
                        sh "git branch"
                        sh "git config --list"
                        sh "git remote set-url origin https://${USER}:${PASS}@gitlab.com/gamal-elhaddad/jenkins-cicd.git"
                        sh "git add ."
                        sh 'git commit -m "jenkins/ci:version bump"'
                        sh 'git push origin HEAD:jenkins-jobs'
                    }
                }
            }
        }
    }   
}
